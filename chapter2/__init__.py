from chapter2.webcount.word_count import most_common_word, most_common_word_in_web_page
from chapter2.webcount.binary_search import binary_search
from .matheval.evaluator import math_eval
