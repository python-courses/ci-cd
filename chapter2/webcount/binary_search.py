def binary_search(needle, sorted_numbers):
    if len(sorted_numbers) == 0:
        raise ValueError

    left = 0
    right = len(sorted_numbers)

    while left <= right:
        mid = (right + left) // 2
        if mid >= len(sorted_numbers):
            raise ValueError("Value not in haystack")

        if needle == sorted_numbers[mid]:
            return mid

        if needle < sorted_numbers[mid]:
            right = mid - 1
        else:
            left = mid + 1

    raise ValueError("Value not in haystack")


if __name__ == '__main__':
    print('Needle found at position {0}'.format(binary_search(5, [1, 2, 3, 4])))
