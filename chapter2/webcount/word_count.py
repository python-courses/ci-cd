import requests


def most_common_words(words, text):
    """
    Returns a sorted list of tuples (word, word_count) for each word from the words list and its
    occurrences in the text. Sorting is based on number of occurrences in reverse order.
    :param words: a list of words
    :param text: a text containing words
    :return: a sorted list of tuples (word, word_count)
    """
    word_frequency = list()  # a list of tuples (word, word_count)
    for word in words:
        word_frequency.append((word, text.count(word)))

    return sorted(word_frequency, key=lambda x: x[1], reverse=True)


def most_common_word(words, text):
    return most_common_words(words, text)[0][0]


def most_common_word_in_web_page(words, url, user_agent=requests):
    response = user_agent.get(url)
    return most_common_word(words, response.text)


if __name__ == '__main__':
    URL = 'https://python.org/'
    word_list = ['Python', 'python', 'programming', 'simple', 'java']
    print(most_common_word_in_web_page(word_list, URL))
