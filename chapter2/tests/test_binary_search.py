from chapter2 import binary_search


def test_found_left_edge():
    assert binary_search(1, [1, 2, 3, 4, 5]) == 0


def test_found_middle():
    assert binary_search(3, [1, 2, 3, 4, 5]) == 2


def test_found_right_edge():
    assert binary_search(5, [1, 2, 3, 4, 5]) == 4


def test_found_other():
    assert binary_search(2, [1, 2, 3, 4, 5]) == 1


def test_exception_not_found():
    from pytest import raises

    with raises(ValueError, message="left out of bounds"):
        binary_search(-1, [1, 2, 3, 4])

    with raises(ValueError, message="right out of bounds"):
        binary_search(5, [1, 2, 3, 4])

    with raises(ValueError, message="not found in middle"):
        binary_search(2, [1, 3, 4])

