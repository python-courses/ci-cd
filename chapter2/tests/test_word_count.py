from chapter2 import most_common_word, most_common_word_in_web_page

def test_most_common_word():
    assert most_common_word(['a', 'b', 'c'], 'abbbcc') \
            == 'b', 'most_common_word with unique answer'


def test_most_common_word_empty_candidate():
    from pytest import raises
    with raises(Exception, message="empty word raises"):
        most_common_word([], 'abc')


def test_most_common_ambiguous_result():
    assert most_common_word(['a', 'b', 'c'], 'ab') \
        in ('a', 'b'), "there might be a tie"


def test_with_test_double():
    class TestResponse:
        text = 'aa bbb c'

    class TestUserAgent:
        def get(self, url=''):
            return TestResponse()

    assert most_common_word_in_web_page(
                ['a', 'b', 'c'],
                'This is not used for testing',
                TestUserAgent()) == 'b'


def test_with_mock_requests():
    from unittest.mock import Mock
    mock_requests = Mock()
    mock_requests.get.return_value.text = 'aa bbb c'

    result = most_common_word_in_web_page(
                ['a', 'b', 'c'],
                'https://python.org/',
                mock_requests)

    assert result == 'b'
